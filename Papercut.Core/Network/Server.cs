﻿/*  
 * Papercut
 *
 *  Copyright © 2008 - 2012 Ken Robertson
 *  Copyright © 2013 - 2014 Jaben Cargman
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

namespace Papercut.Core.Network
{
    using System;
    using System.Net;
    using System.Net.Sockets;

    using Autofac.Features.Indexed;

    using Papercut.Core;

    using Serilog;

    public class Server : IServer
    {
        readonly ServerProtocolType _serverProtocolType;

        public ConnectionManager ConnectionManager { get; set; }

        public ILogger Logger { get; set; }

        public Func<IProtocol> ProtocolFactory { get; set; }

        #region Fields

        IPAddress _address;

        bool _isRunning;

        Socket _listener;

        int _port;

        public Server(
            ServerProtocolType serverProtocolType,
            IIndex<ServerProtocolType, Func<IProtocol>> protocolFactory,
            ConnectionManager connectionManager,
            ILogger logger)
        {
            ConnectionManager = connectionManager;

            _serverProtocolType = serverProtocolType;
            Logger = logger.ForContext("ServerProtocolType", _serverProtocolType);
            ProtocolFactory = protocolFactory[_serverProtocolType];
        }

        #endregion

        #region Public Methods and Operators

        public void Listen(string ip, int port)
        {
            Stop();
            SetEndpoint(ip, port);
            Start();
        }

        public void Stop()
        {
            if (!_isRunning) return;

            Logger.Information("Stopping Server...");

            try
            {
                // Turn off the running bool
                _isRunning = false;

                // Stop the listener
                _listener.Close();

                ConnectionManager.CloseAll();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Exception Stopping Server");
            }
        }

        #endregion

        #region Methods

        protected void CreateListener()
        {
            try
            {
                // If the listener isn't null, close before rebinding
                if (_listener != null) _listener.Close();

                // Bind to the listening port
                _listener = new Socket(
                    AddressFamily.InterNetwork,
                    SocketType.Stream,
                    ProtocolType.Tcp);

                _listener.Bind(new IPEndPoint(_address, _port));
                _listener.Listen(10);
                _listener.BeginAccept(OnClientAccept, null);

                Logger.Information(
                    "Server Ready - Listening for new connections {Address}:{UIPort}",
                    _address,
                    _port);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Exception in Starting Server");
                throw;
            }
        }

        protected void SetEndpoint(string ip, int port)
        {
            // Load IP/UIPort settings
            if (string.IsNullOrWhiteSpace(ip)
                || string.Equals(ip, "any", StringComparison.OrdinalIgnoreCase)) _address = IPAddress.Any;
            else _address = IPAddress.Parse(ip);

            _port = port;
        }

        void OnClientAccept(IAsyncResult ar)
        {
            try
            {
                Socket clientSocket = _listener.EndAccept(ar);
                ConnectionManager.CreateConnection(clientSocket, ProtocolFactory());
            }
            catch (ObjectDisposedException)
            {
                // This can occur when stopping the service.  Squash it, it only means the listener was stopped.
            }
            catch (ArgumentException)
            {
                // This can be thrown when updating settings and rebinding the listener.  It mainly means the IAsyncResult
                // wasn't generated by a BeginAccept event.
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Exception in Server.OnClientAccept");
            }
            finally
            {
                if (_isRunning)
                {
                    try
                    {
                        _listener.BeginAccept(OnClientAccept, null);
                    }
                    catch
                    {
                        // This normally happens when trying to rebind to a port that is taken
                    }
                }
            }
        }

        void Start()
        {
            Logger.Information("Starting Server {ProtocolType}", _serverProtocolType);

            try
            {
                // Set it as starting
                _isRunning = true;

                // Create and start new listener socket
                CreateListener();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Exception Starting Server");
                throw;
            }
        }

        #endregion

        public void Dispose()
        {
            Stop();
        }
    }
}