namespace Papercut.Links
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text.RegularExpressions;

  internal class LinkExtractor
  {
    private string message;
    private static readonly Regex regex = new Regex("\"(http://[^\"]*)\"");

    public LinkExtractor(string message)
    {
      this.message = message;
    }

    public IEnumerable<string> GetLinks()
    {
      var matchCollection = regex.Matches(this.message);

      return matchCollection.Cast<Match>()
        .Select(m => m.Groups[1].Value.ToLower())
        .Where(this.IsLegitimateLink);
    }

    private bool IsLegitimateLink(string link)
    {
      return link.IndexOf("RegisterEmailOpened", StringComparison.InvariantCultureIgnoreCase) > 0 ||
             (link.IndexOf("RedirectUrlPage", StringComparison.InvariantCultureIgnoreCase) > 0 && link.IndexOf("Unsubscribe", StringComparison.InvariantCultureIgnoreCase) < 1);
    }
  }
}