namespace Papercut.Links
{
  internal class LinkOpener
  {
    private readonly string link;

    public LinkOpener(string link)
    {
      this.link = link;
    }

    public void Open()
    {
      var httpWebRequest = System.Net.WebRequest.Create(this.link) as System.Net.HttpWebRequest;
      if (httpWebRequest == null)
      {
        return;
      }

      httpWebRequest.CookieContainer = new System.Net.CookieContainer();
      System.Net.WebResponse webResponse = httpWebRequest.GetResponse();
      using (var responseStream = webResponse.GetResponseStream())
      {
        var buffer = new byte[256];
        while (true)
        {
          if (responseStream.Read(buffer, 0, buffer.Length) < 1)
          {
            break;
          }
        }
      }
    }
  }
}