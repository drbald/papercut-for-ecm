﻿namespace Papercut.Links
{
  using System;
  using System.Collections.Concurrent;
  using System.Threading;

  // Will use this class later to setup frequency of clicking links etc.

  public sealed class LinksQueue : IDisposable
  {
    private static LinksQueue instance;
    public static LinksQueue Instance
    {
      get
      {
        return instance ?? (instance = new LinksQueue());
      }
    }

    public ConcurrentQueue<string> Queue { get; private set; }
    private Thread thread;
    private bool stopThread;

    private LinksQueue()
    {
      this.Queue = new ConcurrentQueue<string>();
      this.thread = new Thread(this.ProcessQueue);
      this.thread.Start();
    }

    private void ProcessQueue()
    {
      while (true)
      {
        string link;
        if (this.Queue.TryDequeue(out link))
        {
          new LinkOpener(link).Open();
        }
        else if (this.stopThread)
        {
          break;
        }
      }
    }

    public void Dispose()
    {
      this.stopThread = true;
      this.thread.Join(10000);
    }
  }
}