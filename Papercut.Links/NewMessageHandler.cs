﻿namespace Papercut.Links
{
  using System;
  using MimeKit;
  using Papercut.Core.Helper;
  using Papercut.Core.Message;

  public class NewMessageHandler
  {
    public MimeMessageLoader MimeMessageLoader { get; set; }
    public MessageRepository MessageRepository { get; set; }

    public NewMessageHandler(MimeMessageLoader mimeMessageLoader, MessageRepository messageRepository)
    {
      this.MimeMessageLoader = mimeMessageLoader;
      this.MessageRepository = messageRepository;

      messageRepository.NewMessage += (sender, e) => this.MimeMessageLoader.Get(e.NewMessage).Subscribe(this.ExtractLinks, () => { });
    }

    public void ExtractLinks(MimeMessage mimeMessage)
    {
      var linkExtractor = new LinkExtractor(mimeMessage.BodyParts.GetMainBodyTextPart().Text);
      foreach (var link in linkExtractor.GetLinks())
      {
        new LinkOpener(link).Open();
        //LinksQueue.Instance.Queue.Enqueue(link);
      }
    }
  }
}
